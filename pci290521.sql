-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 02:59 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pci290521`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/05/26 17:00:05', '2021-05-26', '2021-05-26', 1, 'Abbott'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/05/26 17:00:30', '2021-05-26', '2021-05-26', 1, 'Abbott'),
(3, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/05/26 17:08:38', '2021-05-26', '2021-05-26', 1, 'Abbott'),
(4, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021/05/29 18:02:08', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(5, 'Arun James', 'arun.nelluvelil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/29 18:14:46', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(6, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/29 18:14:47', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(7, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/05/29 18:25:52', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(8, 'Laxmi H Shetty', 'drlaxmihsn@gmail.com', 'Bengaluru', 'Sri Jayadeva institute of cardiovascular sciences and research', NULL, NULL, '2021/05/29 18:33:33', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(9, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/05/29 18:34:01', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(10, 'DR VIJAYKUMAR JR', 'dr.vijaykumarjr@gmail.com', 'Bengaluru', 'Sri Jayadeva Hospital', NULL, NULL, '2021/05/29 18:34:36', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(11, 'Harsha Basappa', 'drharshabasappa@gmail.com', 'Mysore', 'SJIC&R', NULL, NULL, '2021/05/29 18:40:21', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(12, 'Laxmi H Shetty', 'drlaxmihsn@gmail.com', 'Bengaluru', 'Sri Jayadeva institute of cardiovascular sciences and research', NULL, NULL, '2021/05/29 18:42:36', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(13, 'Dr Santhosh K', 'drsanthoshk@gmail.com', 'Mysore', 'SJICSR', NULL, NULL, '2021/05/29 18:47:20', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(14, 'B C Srinivas', 'bcsrinivas@rediffmail.com', 'Bangalore', 'Jayadeva institute', NULL, NULL, '2021/05/29 18:51:04', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(15, 'Dr Veena Nanjappa', 'veenananjappa@yahoo.co.in', 'Mysore', 'SJICSR', NULL, NULL, '2021/05/29 18:52:55', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(16, 'Srinivasa K H', 'drkhsrinivas@yahoo.co.in', 'Bangalore', 'SJICR', NULL, NULL, '2021/05/29 18:56:16', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(17, 'Ksr', 'ksr@gmail.com', 'ksr', 'ksr', NULL, NULL, '2021/05/29 18:58:04', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(18, 'harsha basappa', 'drharshabasappa@gmail.com', 'Mysore', 'SJIC&R', NULL, NULL, '2021/05/29 18:58:59', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(19, 'Dr KSR', 'kkk@gmail.com', '999999ksr', '555ksr', NULL, NULL, '2021/05/29 18:59:15', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(20, 'Dr K S Ravindranath', 'drksravi@gmail.com', 'Bengaluru', 'Sjic', NULL, NULL, '2021/05/29 19:04:07', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(21, 'Dr Santhosh K', 'drsanthoshk@gmail.com', 'Mysore', 'SJICSR', NULL, NULL, '2021/05/29 19:08:31', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(22, 'Dr Santhosh K', 'drsanthoshk@gmail.com', 'Mysore', 'SJICSR', NULL, NULL, '2021/05/29 19:15:00', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(23, 'Arun James', 'arun.nelluvelil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/29 20:30:56', '2021-05-29', '2021-05-29', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'ANAND PALAKSHACHAR', 'anandpucms@gmail.com', 'HEMODYNAMIC COMPROMISE WITH PACING AND LM OCCLUSION ? NOT PROBLEM ? ', '2021-05-29 19:14:56', 'Abbott', 0, 0),
(2, 'Sunil Kumar S', 'sunilbmc98@gmail.com', 'ostial lesion why was cutting balloon not used ?. significant fibrosis could be the reason . If CB PTCA was done stenting could have been easier without the need for pacing', '2021-05-29 19:15:40', 'Abbott', 0, 0),
(3, 'Dr CB Keshavamurthy', 'cbkeshavamurthy@gmail.com', 'Are you comfortable pacing a patient at 180 bpm, with a severe left main stenosis?', '2021-05-29 19:15:57', 'Abbott', 0, 0),
(4, 'Sudhakar Rao', 'msudhakar88@gmail.com', 'with 7 f guide does triple kissing need all N balloons', '2021-05-29 19:32:09', 'Abbott', 0, 0),
(5, 'Sudhakar Rao', 'msudhakar88@gmail.com', 'is it worth trying a av fistula failure due to maturations issues which is around 20 days', '2021-05-29 19:45:30', 'Abbott', 0, 0),
(6, 'Nagamani A C', 'drnagamani_c@yahoo.co.in', '  Audio not clear', '2021-05-29 20:11:32', 'Abbott', 0, 0),
(7, 'Naga Srinivaas', 'drakondi.ns@gmail.gom', 'Why upfront 2 stents ? LCx disease is < 10 mm ', '2021-05-29 20:15:45', 'Abbott', 0, 0),
(8, 'Naga Srinivaas', 'drakondi.ns@gmail.gom', 'Any imaging used in this case post pci ? ', '2021-05-29 20:18:56', 'Abbott', 0, 0),
(9, 'Nagaraja Moorthy', 'drnagaraj_moorthy@yahoo.com', 'Great cases . Great demo Dr Vijay\r\nGreat learning and teaching ðŸ‘ðŸ‘', '2021-05-29 20:53:29', 'Abbott', 0, 0),
(10, 'Anindya Sundar Trivedi', 'anindyaagmc@gmail.com', 'Thank you teachers', '2021-05-29 20:57:21', 'Abbott', 0, 0),
(11, 'Praveen kumar', 'jipmer.praveen@gmail.com', 'After putting proglide patient can be discharged on same day ', '2021-05-29 20:58:29', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-26 16:56:58', '2021-05-26 16:56:58', '2021-05-26 16:57:08', 0, 'Abbott', '92d0304f747167eaabab264dd86ccb6d'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-26 16:57:58', '2021-05-26 16:57:58', '2021-05-26 18:27:58', 0, 'Abbott', '409432c3d4f5b90fce74674514d9cfd9'),
(3, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-05-27 16:15:09', '2021-05-27 16:15:09', '2021-05-27 17:45:09', 0, 'Abbott', '52e5f9eb45d84bc459579b16af951e3f'),
(4, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-28 15:50:13', '2021-05-28 15:50:13', '2021-05-28 17:20:13', 0, 'Abbott', '6bc4328848c6a32a5efb7f9fd34b5a26'),
(5, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-28 15:59:14', '2021-05-28 15:59:14', '2021-05-28 17:29:14', 0, 'Abbott', '6f5d02d645ba416315c8acd9fb38f704'),
(6, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-28 16:27:03', '2021-05-28 16:27:03', '2021-05-28 16:27:19', 0, 'Abbott', 'f31bdddc7546e4a49bc47a020ee5dec9'),
(7, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-28 19:39:27', '2021-05-28 19:39:27', '2021-05-28 21:09:27', 0, 'Abbott', '937856ce793c7b29e88f521458ebbc43'),
(8, 'Niranjan reddy', 'drniranjan11@gmail.com', 'Kolar', 'RLJNH', NULL, NULL, '2021-05-29 09:18:10', '2021-05-29 09:18:10', '2021-05-29 10:48:10', 0, 'Abbott', 'd43657df15d86b6599bd9a2104e36a0b'),
(9, 'Puneet Shah', 'puneetishere2050@gmail.com', 'Bengaluru (Bangalore) Urban', 'Sjicr', NULL, NULL, '2021-05-29 11:49:51', '2021-05-29 11:49:51', '2021-05-29 11:49:56', 0, 'Abbott', '5e2313e4bf8a23e7215890e5b94ade5f'),
(10, 'Lavanya', 'lavanyalavu1018@gmail.com', 'Bengaluru Urban', 'Jayadeva', NULL, NULL, '2021-05-29 11:55:08', '2021-05-29 11:55:08', '2021-05-29 13:25:08', 0, 'Abbott', '465d9322f81766999b44c7cb48766910'),
(11, 'Govardha ', 'govikusu@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-05-29 12:56:27', '2021-05-29 12:56:27', '2021-05-29 14:26:27', 0, 'Abbott', 'ded9f4588451c0c8f45914f387f139ee'),
(12, 'Natraj ', 'drnatrajsetty75@gmail.com', 'Bangalore ', 'Jayadeva ', NULL, NULL, '2021-05-29 14:45:06', '2021-05-29 14:45:06', '2021-05-29 16:15:06', 0, 'Abbott', 'a0838a70b2afae6174c95442977e0c11'),
(13, 'Rajendra', 'drrajendraph@gmail.com', 'Bangalore', 'Jayadeva hospital ', NULL, NULL, '2021-05-29 14:57:43', '2021-05-29 14:57:43', '2021-05-29 14:57:48', 0, 'Abbott', '58014f0df59ff02471ae294fedef98d2'),
(14, 'Anju Kottayan', 'anjuaanand722@gmail.com', 'Bangalore', 'Sri Jayadeva Institute of Cardiology', NULL, NULL, '2021-05-29 16:45:48', '2021-05-29 16:45:48', '2021-05-29 18:15:48', 0, 'Abbott', 'e2bc4c733648f43469ab30e3169644d3'),
(15, 'Gurvinder Singh', 'gurvinder.x.singh@abbott.com', 'Delhi', 'Abbott', NULL, NULL, '2021-05-29 17:02:27', '2021-05-29 17:02:27', '2021-05-29 18:32:27', 0, 'Abbott', '4ad230020b1b3cb326afddc47fe18eac'),
(16, 'Nishanth S', 'test@test.com', 'test', 'test', NULL, NULL, '2021-05-29 17:26:33', '2021-05-29 17:26:33', '2021-05-29 17:26:47', 0, 'Abbott', '6ca90b72e76081216e84588b57c00ae1'),
(17, 'Veena', 'veenananjappa@yahoo.co.in', 'Mysore', 'SJICSR', NULL, NULL, '2021-05-29 17:46:07', '2021-05-29 17:46:07', '2021-05-29 19:16:07', 0, 'Abbott', '1e132d2eb1162ad2a69488688eec9c3f'),
(18, 'Dileep', 'harathiyenumala@gmail.com', 'bengaluru', 'sjic', NULL, NULL, '2021-05-29 17:59:05', '2021-05-29 17:59:05', '2021-05-29 19:29:05', 0, 'Abbott', '47735e78fe7c9fe0233f7636283f27bb'),
(19, 'Kuldeep Totawar', 'krtworld@gmail.com', 'Bangalore 560069', 'SJICR', NULL, NULL, '2021-05-29 18:20:54', '2021-05-29 18:20:54', '2021-05-29 19:50:54', 1, 'Abbott', '1df53ae0bb6b1fc4e94aba5ee7e983e2'),
(20, 'DR VIJAYKUMAR JR', 'dr.vijaykumarjr@gmail.com', 'Bengaluru', 'Sri Jayadeva Hospital', NULL, NULL, '2021-05-29 18:31:03', '2021-05-29 18:31:03', '2021-05-29 20:01:03', 1, 'Abbott', '77959c69f484128b09f13730573fc512'),
(21, 'Dr Santhosh K', 'drsanthoshk@gmail.com', 'Mysore', 'SJICSR', NULL, NULL, '2021-05-29 18:33:18', '2021-05-29 18:33:18', '2021-05-29 20:03:18', 1, 'Abbott', '5d886b4a56b46fa444185c62f49362a5'),
(22, 'DR VIJAYKUMAR JR', 'dr.vijaykumarjr@gmail.com', 'Bengaluru', 'Sri Jayadeva Hospital', NULL, NULL, '2021-05-29 18:33:45', '2021-05-29 18:33:45', '2021-05-29 20:03:45', 1, 'Abbott', 'c08dbba07954ab2db3b2795ec21cc61e'),
(23, 'LOKESH PRAMOD CHAUDHARI', 'lokeshataipg@rediffmail.com', 'banglore', 'Sjic', NULL, NULL, '2021-05-29 18:34:57', '2021-05-29 18:34:57', '2021-05-29 20:04:57', 1, 'Abbott', 'c6cc13e2269d10e27e626eeb5fc928fe'),
(24, 'ANAND PALAKSHACHAR', 'anandpucms@gmail.com', 'BANGALORE', 'JAYADEVA', NULL, NULL, '2021-05-29 18:37:45', '2021-05-29 18:37:45', '2021-05-29 20:07:45', 1, 'Abbott', '8d45bd7a9b08307f10ccaf54731aa0e5'),
(25, 'Abdul', 'abdulraheem42@gmail.com', 'Raichur', 'Sri Jayadeva institute of cardiovascular science and research.', NULL, NULL, '2021-05-29 18:40:34', '2021-05-29 18:40:34', '2021-05-29 20:10:34', 1, 'Abbott', '263dbf31d01c43d9933f06c05f273fcd'),
(26, 'Dr Jivitesh Satija', 'jiviteshsatija@gmail.com', 'Bangalore', 'SJICR', NULL, NULL, '2021-05-29 18:42:32', '2021-05-29 18:42:32', '2021-05-29 20:12:32', 1, 'Abbott', '1677aa07b3de121042993fab39643cdd'),
(27, 'Yogesh  Musale', 'yogesh.sitarammusale@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-29 18:45:50', '2021-05-29 18:45:50', '2021-05-29 20:15:50', 1, 'Abbott', '0dde12f4c648c74eb5c31837ce8cd025'),
(28, 'Dr Veena Nanjappa', 'veenananjappa@yahoo.co.in', 'Mysore', 'SJICSR', NULL, NULL, '2021-05-29 18:47:25', '2021-05-29 18:47:25', '2021-05-29 20:17:25', 1, 'Abbott', '716aab034fd967c10260d349b8a1086d'),
(29, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-05-29 18:48:01', '2021-05-29 18:48:01', '2021-05-29 20:18:01', 1, 'Abbott', '8f7e0caae645fe8f40752826bff588aa'),
(30, 'Govardha ', 'govikusu@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-05-29 18:52:05', '2021-05-29 18:52:05', '2021-05-29 20:22:05', 1, 'Abbott', 'c30689a97080650cbd0bebbdc176f28b'),
(31, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 18:53:28', '2021-05-29 18:53:28', '2021-05-29 20:23:28', 1, 'Abbott', '3e6a526742414775d1575e144870f8b5'),
(32, 'Abdul', 'abdulraheem42@gmail.com', 'Raichur', 'Sri Jayadeva institute of cardiovascular science and research.', NULL, NULL, '2021-05-29 18:55:49', '2021-05-29 18:55:49', '2021-05-29 20:25:49', 1, 'Abbott', 'a87cea7ab5b6e460b704337f20e56e0f'),
(33, 'Abhilash ', 'abhilash.mohanan@abbott.com', 'Bangalore ', 'Abbott', NULL, NULL, '2021-05-29 18:56:09', '2021-05-29 18:56:09', '2021-05-29 20:26:09', 1, 'Abbott', 'ea3d1a30d23db1ef1124e8f942f333be'),
(34, 'Siddhant Trehan', 'siddhanttrehan@gmail.com', 'Bengaluru', 'Jayadeva', NULL, NULL, '2021-05-29 18:56:42', '2021-05-29 18:56:42', '2021-05-29 20:26:42', 1, 'Abbott', '0201c043a21f55846479bec76d7fe417'),
(35, 'Dr CB Keshavamurthy', 'cbkeshavamurthy@gmail.com', 'Mysore', 'columbia asia hospital', NULL, NULL, '2021-05-29 18:56:44', '2021-05-29 18:56:44', '2021-05-29 20:26:44', 1, 'Abbott', '6c9a8580edba2756c75fecdabcca4d06'),
(36, 'Hariprasad Rao', 'prasadhari88@gmail.com', 'Banglore', 'Sjicr', NULL, NULL, '2021-05-29 18:56:44', '2021-05-29 18:56:44', '2021-05-29 20:26:44', 1, 'Abbott', 'ad278fc60e5d17b816329de83de16c8b'),
(37, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-05-29 18:56:55', '2021-05-29 18:56:55', '2021-05-29 20:26:55', 1, 'Abbott', '4ab284242e2fb3c96dfa360401cd6624'),
(38, 'veenu john', 'veenu.remaliya@gmail.com', 'Mysore', 'Apollo hospital ', NULL, NULL, '2021-05-29 18:57:11', '2021-05-29 18:57:11', '2021-05-29 19:21:57', 0, 'Abbott', '0054101bd90ce06d20b77ea696af1232'),
(39, 'Dr B Dinesh', 'dr.dinesha@gmail.com', 'Mysore', 'Jayadeva hospital', NULL, NULL, '2021-05-29 18:58:23', '2021-05-29 18:58:23', '2021-05-29 20:28:23', 1, 'Abbott', '6c5601d7d7c58527480233802a3e1b62'),
(40, 'Sudhakar Rao', 'msudhakar88@gmail.com', 'Manipal', 'Kmc', NULL, NULL, '2021-05-29 18:59:48', '2021-05-29 18:59:48', '2021-05-29 20:29:48', 1, 'Abbott', '5c3e5b03dfd2ba2f52d006b344967f40'),
(41, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 18:59:55', '2021-05-29 18:59:55', '2021-05-29 20:29:55', 1, 'Abbott', '1069e6e631cfe23d10f29bb7030d872e'),
(42, 'Ravi Math', 'ravismath76@gmail.com', 'Bangalore', 'Jayadeva hospital', NULL, NULL, '2021-05-29 19:00:16', '2021-05-29 19:00:16', '2021-05-29 20:30:16', 1, 'Abbott', 'd9393d815f7286a05070dce3537138ef'),
(43, 'JIJU JOHN', 'jijujohn27@gmail.com', 'Bangalore', 'Jayadeva hospital', NULL, NULL, '2021-05-29 19:01:05', '2021-05-29 19:01:05', '2021-05-29 20:31:05', 1, 'Abbott', '8ffef77d1f4acc06d99266fff9a54ee9'),
(44, 'ANAND', 'anandpucms@gmail.com', 'bangalore', 'jayadeva', NULL, NULL, '2021-05-29 19:01:42', '2021-05-29 19:01:42', '2021-05-29 20:31:42', 1, 'Abbott', '157b0e4aaa8976e244376b73cc544ad1'),
(45, 'LOKESH PRAMOD CHAUDHARI', 'lokeshataipg@rediffmail.com', 'banglore', 'Sjic', NULL, NULL, '2021-05-29 19:01:52', '2021-05-29 19:01:52', '2021-05-29 20:31:52', 1, 'Abbott', 'd90c1631a433b7a2e48f30d044bcaf52'),
(46, 'Sharathbabu N M', 'nmsbabu18@yahoo.com', 'Mysuru', 'SJICSR', NULL, NULL, '2021-05-29 19:02:03', '2021-05-29 19:02:03', '2021-05-29 20:32:03', 1, 'Abbott', 'b6d63fc3141120afcdbe6dfd8a964604'),
(47, 'Sharad Masudi', 'sharad.masudi@gmail.com', 'Bengaluru', 'Sri Jayadeva institute of Cardiology', NULL, NULL, '2021-05-29 19:02:03', '2021-05-29 19:02:03', '2021-05-29 20:32:03', 1, 'Abbott', '5e624dceafd72cc195676fe16a04fab4'),
(48, 'Yeriswamy Mogalahally channabasappa', 'drswim2003@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-05-29 19:02:12', '2021-05-29 19:02:12', '2021-05-29 20:32:12', 1, 'Abbott', '693e96e6b25b3034393ed305724ee44c'),
(49, 'Nagamani A C', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva hospital ', NULL, NULL, '2021-05-29 19:02:17', '2021-05-29 19:02:17', '2021-05-29 20:32:17', 1, 'Abbott', '111ee1aa2592f7e644bcada835b8a2a6'),
(50, 'Nithin', 'nithinrao972@gmail.com', 'Banglore', 'Sjicr', NULL, NULL, '2021-05-29 19:02:21', '2021-05-29 19:02:21', '2021-05-29 20:32:21', 1, 'Abbott', '9a138531b0db62894fa0936604b18d2f'),
(51, 'Nagaraja Moorthy', 'drnagaraj_moorthy@yahoo.com', 'Bangalore ', 'Sjicr ', NULL, NULL, '2021-05-29 19:02:27', '2021-05-29 19:02:27', '2021-05-29 20:32:27', 1, 'Abbott', '25a80c0497d01e84cfc2643baa32bcd3'),
(52, 'Siddhant', 'siddhanttrehan@gmail.com', 'Bengaluru', 'Jayadeva Hospital', NULL, NULL, '2021-05-29 19:02:31', '2021-05-29 19:02:31', '2021-05-29 20:32:31', 1, 'Abbott', '98f610d481d71e2608165ce41043e64d'),
(53, 'Tejeshwar reddy', 'rede237@gmail.com', 'Ballari', 'Jayadeva hospital', NULL, NULL, '2021-05-29 19:02:42', '2021-05-29 19:02:42', '2021-05-29 19:08:35', 0, 'Abbott', 'fc91782c66b53a6ac6dfe55db5fc4efc'),
(54, 'Arun James', 'arun.nelluvelil@abbott.com', 'BANGALORE', 'Av', NULL, NULL, '2021-05-29 19:02:59', '2021-05-29 19:02:59', '2021-05-29 20:32:59', 1, 'Abbott', 'c3403c9c253a9144fe746dfe3795bd4d'),
(55, 'LAKSHMI NAVYA', 'lakshminavyach@gmail.com', 'Bengaluru', 'Sri jayadeva institute of cardiovascular sciences', NULL, NULL, '2021-05-29 19:03:21', '2021-05-29 19:03:21', '2021-05-29 20:33:21', 1, 'Abbott', '5c55034ac135f499f521db0b9a30e04a'),
(56, 'Govardha ', 'govikusu@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-05-29 19:03:23', '2021-05-29 19:03:23', '2021-05-29 20:33:23', 1, 'Abbott', 'a3d4508b99fe11cd9d25d61cc34390a7'),
(57, 'Dileep', 'harathiyenumala@gmail.com', 'bengaluru', 'sjic', NULL, NULL, '2021-05-29 19:03:49', '2021-05-29 19:03:49', '2021-05-29 20:33:49', 1, 'Abbott', '66bece0d94404616a2bc9d9b20396db4'),
(58, 'Indhu prakash ', 'indhuprakash06@gmail.com', 'Bangalore ', 'Jayadeva hospital ', NULL, NULL, '2021-05-29 19:04:27', '2021-05-29 19:04:27', '2021-05-29 20:34:27', 1, 'Abbott', '6298bd00f201c8c3344e4f7d3ce78b53'),
(59, 'Dinesh B', 'dr.dinesha@gn', 'Mysore ', 'Jayadeva hospital ', NULL, NULL, '2021-05-29 19:04:47', '2021-05-29 19:04:47', '2021-05-29 20:34:47', 1, 'Abbott', '03e8909a5c87d861b53303dfaf98d82c'),
(60, 'Dr Anesh Jain', 'areconsultancy@gmail.com', 'Bengaluru', 'SJICR', NULL, NULL, '2021-05-29 19:05:06', '2021-05-29 19:05:06', '2021-05-29 20:35:06', 1, 'Abbott', '57c4239ecacb6e219a86b33b8ba2b26c'),
(61, 'Girish kolambe', 'girishkol4262@gmail.com', 'Banglore', 'Jayadeva', NULL, NULL, '2021-05-29 19:05:09', '2021-05-29 19:05:09', '2021-05-29 20:35:09', 1, 'Abbott', '861998caf88f481f15f907a43b78898e'),
(62, 'PADMAJA', 'padmar091@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-05-29 19:05:09', '2021-05-29 19:05:09', '2021-05-29 20:35:09', 1, 'Abbott', 'bdaa9e8e442b69853988558da1bd992e'),
(63, 'Dileep', 'harathiyenumala@gmail.com', 'bengaluru', 'sjic', NULL, NULL, '2021-05-29 19:05:21', '2021-05-29 19:05:21', '2021-05-29 20:35:21', 1, 'Abbott', '137996293238d457b950a07e953a6b94'),
(64, 'dr.mahesh', 'maheshvaidya26@gmail.com', 'SHIMOGA', 'HRUDAY SPECIALITY CLINIC', NULL, NULL, '2021-05-29 19:05:28', '2021-05-29 19:05:28', '2021-05-29 20:35:28', 1, 'Abbott', 'a87252708e51fbd40c42822ea4b652c8'),
(65, 'Sunil Kumar S', 'sunilbmc98@gmail.com', 'Bangalore', 'Columbia Asia Hospital', NULL, NULL, '2021-05-29 19:05:45', '2021-05-29 19:05:45', '2021-05-29 20:35:45', 1, 'Abbott', 'e9e980ef1599bed013df37049aca515e'),
(66, 'Jayashree Kharge', 'khargej@gmail.com', 'Bangalore', 'SJICSR', NULL, NULL, '2021-05-29 19:05:55', '2021-05-29 19:05:55', '2021-05-29 20:35:55', 1, 'Abbott', 'c166bd63ba4be30d4594aae8b7b35af4'),
(67, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-29 19:06:01', '2021-05-29 19:06:01', '2021-05-29 19:06:25', 0, 'Abbott', '87de6c23ecee548dbcb743fc1a57c8e1'),
(68, 'Chetan', 'chetanhb09@gmail.com', 'Banglore', 'SJICR', NULL, NULL, '2021-05-29 19:06:09', '2021-05-29 19:06:09', '2021-05-29 20:36:09', 1, 'Abbott', '27fa3831f3d4caa157ef415ff5e22700'),
(69, 'Nishanth', 'kkk@gmail.com', 'Blore', 'SJICR', NULL, NULL, '2021-05-29 19:06:10', '2021-05-29 19:06:10', '2021-05-29 20:36:10', 1, 'Abbott', 'fa7d6a03384fb3223c66080459c9bc00'),
(70, 'Anindya Sundar Trivedi', 'anindyaagmc@gmail.com', 'Banglore', 'SJICR', NULL, NULL, '2021-05-29 19:06:45', '2021-05-29 19:06:45', '2021-05-29 20:36:45', 1, 'Abbott', 'aa17367d00abe2fd23f737170aea4c7e'),
(71, 'sridhar', 'drnsridhara@gmail.com', 'bangalore', 'fortis', NULL, NULL, '2021-05-29 19:07:45', '2021-05-29 19:07:45', '2021-05-29 20:37:45', 1, 'Abbott', '7bd8a952d688743be971a71f1118efa4'),
(72, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-29 19:07:50', '2021-05-29 19:07:50', '2021-05-29 20:37:50', 1, 'Abbott', '1fde55d7bd06fdf424bc78f694cf7814'),
(73, 'Dr Jivitesh Satija', 'jiviteshsatija@gmail.com', 'Bangalore', 'SJICR', NULL, NULL, '2021-05-29 19:08:06', '2021-05-29 19:08:06', '2021-05-29 20:38:06', 1, 'Abbott', '0878a4cc925d9eec62ee6789b21354e9'),
(74, 'Lingaiah', 'lingaiah.gaddi@abbott.com', 'Hyderabad', 'Abbott', NULL, NULL, '2021-05-29 19:08:43', '2021-05-29 19:08:43', '2021-05-29 20:38:43', 1, 'Abbott', 'dbea7d644865f0a5bb154bb93ce8cf96'),
(75, 'Dr. Amith R', 'amith1ind@gmail.com', 'Bangalore ', 'SJICR ', NULL, NULL, '2021-05-29 19:08:52', '2021-05-29 19:08:52', '2021-05-29 20:38:52', 1, 'Abbott', 'd4a352672877a92f3fd9fdead7d99e50'),
(76, 'Srinivas', 'rede237@gmail.com', 'Bellary', 'Jayadeva hospital', NULL, NULL, '2021-05-29 19:09:13', '2021-05-29 19:09:13', '2021-05-29 20:39:13', 1, 'Abbott', 'f4304aadc1a1a46bd8d8c266bd088e4b'),
(77, 'Beeresha ', 'beereshp@yahoo.co.in', 'Bengaluru ', 'Sjicr ', NULL, NULL, '2021-05-29 19:09:25', '2021-05-29 19:09:25', '2021-05-29 20:39:25', 1, 'Abbott', 'be04acfa389505c7da2811798b9dd5bc'),
(78, 'Nagesh cm', 'anandpucms@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-05-29 19:09:28', '2021-05-29 19:09:28', '2021-05-29 20:39:28', 1, 'Abbott', '136483d3d040ee98dc8460393fc1e88e'),
(79, 'NISHANTH', 'kkk@gmail.com', 'Blore', 'Sjic', NULL, NULL, '2021-05-29 19:09:32', '2021-05-29 19:09:32', '2021-05-29 20:39:32', 1, 'Abbott', '282dd6d0c37498ecfc875330a558321b'),
(80, 'Aditya Mulgaonkar', 'adityamulgaonkar121@gmail.com', 'Bangalore', 'SJICS&R', NULL, NULL, '2021-05-29 19:09:38', '2021-05-29 19:09:38', '2021-05-29 20:39:38', 1, 'Abbott', 'ce4311fb700ed33ac151ed49598bce1c'),
(81, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 19:10:11', '2021-05-29 19:10:11', '2021-05-29 20:40:11', 1, 'Abbott', 'e58b734bbce31c9e4f10ab720510e535'),
(82, 'Renish ', 'rbera22@gmail.com', 'Bengalore ', 'Sri Jayadeva institute of cardiovascular sciences ', NULL, NULL, '2021-05-29 19:10:26', '2021-05-29 19:10:26', '2021-05-29 20:40:26', 1, 'Abbott', '29db0c8d9fd95b613f5020b91e9192e5'),
(83, 'Yeriswamy Mogalahally channabasappa', 'drswim2003@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-05-29 19:10:26', '2021-05-29 19:10:26', '2021-05-29 20:40:26', 1, 'Abbott', 'fb03a2bbf4e13383b5ef9e55e57258cb'),
(84, 'Mohan h n', 'hn.mohan8@gmail.com', 'Bangalore', 'Sjic &r', NULL, NULL, '2021-05-29 19:10:28', '2021-05-29 19:10:28', '2021-05-29 20:40:28', 1, 'Abbott', '5d0bb287ec450265c7d3c386842cc61d'),
(85, 'Bhaskar ', 'bhaskarsrao@yahoo.com', 'Bangalore ', 'sjicsr', NULL, NULL, '2021-05-29 19:11:04', '2021-05-29 19:11:04', '2021-05-29 20:41:04', 1, 'Abbott', '419c1024a0ce0a02c2d0df2635bcbac6'),
(86, 'Mahendra Kumar G', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-05-29 19:11:14', '2021-05-29 19:11:14', '2021-05-29 20:41:14', 1, 'Abbott', '1f06c152a2393a53e581f3b257964de5'),
(87, 'sreedhara ', 'kummas82@gmail.com', 'mysore ', 'sjicr ', NULL, NULL, '2021-05-29 19:11:33', '2021-05-29 19:11:33', '2021-05-29 20:41:33', 1, 'Abbott', '52403e0b2ab7a6e757212f3e57d4d39d'),
(88, 'Hariprasad Rao', 'prasadhari88@gmail.com', 'Banglore', 'Sjicr', NULL, NULL, '2021-05-29 19:11:48', '2021-05-29 19:11:48', '2021-05-29 20:41:48', 1, 'Abbott', '3660e0860a0170a75ac6657d77af003b'),
(89, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 19:12:15', '2021-05-29 19:12:15', '2021-05-29 20:42:15', 1, 'Abbott', 'c84109fc87c2559bce6a4bf88e37ef54'),
(90, 'Dr.Chetana ', 'chetana88@gmail.com', 'Mysore', 'SJICSR', NULL, NULL, '2021-05-29 19:12:55', '2021-05-29 19:12:55', '2021-05-29 20:42:55', 1, 'Abbott', 'c413adcf46576950128299ad07c07c65'),
(91, 'Ck', 'chetanhb09@gmail.com', 'Banglore', 'SJICR', NULL, NULL, '2021-05-29 19:13:22', '2021-05-29 19:13:22', '2021-05-29 20:43:22', 1, 'Abbott', '5554a575d56810683cb05cd596b7ef0f'),
(92, 'Ramprasad Kadiyala', 'dr.ramprasadk@yahoo.com', 'Bangalore, India', 'Sjicr', NULL, NULL, '2021-05-29 19:13:50', '2021-05-29 19:13:50', '2021-05-29 20:43:50', 1, 'Abbott', 'c4a009105993a034a3dd7bec011187b8'),
(93, 'Beeresha ', 'beereshp@yahoo.co.in', 'Bengaluru ', 'SJICR ', NULL, NULL, '2021-05-29 19:15:14', '2021-05-29 19:15:14', '2021-05-29 20:45:14', 1, 'Abbott', 'a66bc9e62fe5f5c7ebf3684ddbca4617'),
(94, 'Goutam Yelsangikar', 'goutambmc@gmail.com', 'Bangalore', 'SJICSR, Bangalore', NULL, NULL, '2021-05-29 19:15:30', '2021-05-29 19:15:30', '2021-05-29 20:45:30', 0, 'Abbott', 'becf77a08b2161fdb54cb1e3ed131042'),
(95, 'Yogesh Musale', 'yogesh.sitarammusale@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-29 19:16:23', '2021-05-29 19:16:23', '2021-05-29 20:46:23', 0, 'Abbott', '231b58883268a67fa972c256b854daf4'),
(96, 'Ramprasad Kadiyala', 'dr.ramprasadk@yahoo.com', 'Bangalore, India', 'Sjicr', NULL, NULL, '2021-05-29 19:16:41', '2021-05-29 19:16:41', '2021-05-29 20:46:41', 0, 'Abbott', 'ee9e7291caf012d4bcca51cb8ab01b20'),
(97, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 19:16:56', '2021-05-29 19:16:56', '2021-05-29 20:46:56', 0, 'Abbott', '7b7d7d927ce1bef961fc9375e727a421'),
(98, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 19:17:27', '2021-05-29 19:17:27', '2021-05-29 20:47:27', 0, 'Abbott', '4658212874e737f0eb8fb774e57d2b62'),
(99, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-05-29 19:18:05', '2021-05-29 19:18:05', '2021-05-29 20:48:05', 0, 'Abbott', '5038fa124b65e69bdcd744f708708727'),
(100, 'Jeevanraj R P', 'jeevanrajchoukeri@gmail.com', 'bangalore', 'jayadeva', NULL, NULL, '2021-05-29 19:18:07', '2021-05-29 19:18:07', '2021-05-29 20:48:07', 0, 'Abbott', '76309a8a95628fd6d49550b75372521d'),
(101, 'Dr Shankar sp Shankar sira', 'spshankarsira@gmail.com', 'Bangalore', 'JAYADEVA HOSPITAL', NULL, NULL, '2021-05-29 19:18:08', '2021-05-29 19:18:08', '2021-05-29 20:48:08', 0, 'Abbott', '646b82d24170bd79faa0c930bcfbc37a'),
(102, 'Arunkumar ', 'a.rubesh@gmail.com', 'Banagalore ', 'SJICR ', NULL, NULL, '2021-05-29 19:18:49', '2021-05-29 19:18:49', '2021-05-29 20:48:49', 0, 'Abbott', 'ca629335b259f770f0805559f62036a3'),
(103, 'B', 'appa@kunji.com', 'bobbli', 'LKB', NULL, NULL, '2021-05-29 19:19:05', '2021-05-29 19:19:05', '2021-05-29 20:49:05', 0, 'Abbott', '81adda7a59d93dd767111f2e042d9bfc'),
(104, 'RONAK ', 'dr.ronakbhandari@gmail.com', 'Bangalore ', 'SJICR ', NULL, NULL, '2021-05-29 19:19:19', '2021-05-29 19:19:19', '2021-05-29 20:49:19', 0, 'Abbott', '5b9232c984f96af93c5dc81c7031481c'),
(105, 'Dr Shankar sp Shankar sira', 'spshankarsira@gmail.com', 'Bangalore', 'JAYADEVA HOSPITAL', NULL, NULL, '2021-05-29 19:20:57', '2021-05-29 19:20:57', '2021-05-29 20:50:57', 0, 'Abbott', '8d980bcc000d9019439ded475330f781'),
(106, 'Kapil Rangan ', 'kapilrangan@gmail.com', 'Bangalore ', 'Sri Jayadeva Institute ', NULL, NULL, '2021-05-29 19:20:59', '2021-05-29 19:20:59', '2021-05-29 20:50:59', 0, 'Abbott', 'ba57920d504a4a8989541670a190facf'),
(107, 'Krishna', 'sagargmcg@gmail.com', 'Bangalore', 'SJIC', NULL, NULL, '2021-05-29 19:21:40', '2021-05-29 19:21:40', '2021-05-29 20:51:40', 0, 'Abbott', '51a37f4afe4c7b820f5a5430aa02b395'),
(108, 'Gopidi Aparanji', 'gopidiaparanji2k8@gmail.com', 'Bangalore ', 'SJICSR ', NULL, NULL, '2021-05-29 19:21:54', '2021-05-29 19:21:54', '2021-05-29 20:51:54', 0, 'Abbott', 'fa55d3d7b2dc0934d6bbe17b918f530f'),
(109, 'PRAVEEN RAJA', 'praveenrajar6688@gmail.com', 'Bangalore', 'SJICR ', NULL, NULL, '2021-05-29 19:21:58', '2021-05-29 19:21:58', '2021-05-29 20:51:58', 0, 'Abbott', '162820685165bd01d1ba7c5d77599931'),
(110, 'Sadanand', 'sadanand_dm@rediffmail.com', 'Mysore', 'Jayadeva ', NULL, NULL, '2021-05-29 19:22:00', '2021-05-29 19:22:00', '2021-05-29 20:52:00', 0, 'Abbott', '80f4b71353835d53916eaf74dfde8aea'),
(111, 'Babu reddy ', 'drbabu20009@gmail.com', 'Bangalore ', 'Jaydeva ', NULL, NULL, '2021-05-29 19:22:12', '2021-05-29 19:22:12', '2021-05-29 20:52:12', 0, 'Abbott', 'b84368f243d402de3fa574a1767c2907'),
(112, 'Srinidhi Hegde', 'dr.srihegde@gmail.com', 'Mysuru', 'Jayadeva ', NULL, NULL, '2021-05-29 19:22:25', '2021-05-29 19:22:25', '2021-05-29 20:52:25', 0, 'Abbott', '5492b34dd5181a441d619d9c165824ed'),
(113, 'Yeriswamy Mogalahally channabasappa', 'drswim2003@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-05-29 19:22:26', '2021-05-29 19:22:26', '2021-05-29 20:52:26', 0, 'Abbott', 'a34477641bea99c47c4414e521dfef38'),
(114, 'Ramprasad Kadiyala', 'dr.ramprasadk@yahoo.com', 'Bangalore, India', 'Sjicr', NULL, NULL, '2021-05-29 19:23:17', '2021-05-29 19:23:17', '2021-05-29 20:53:17', 0, 'Abbott', 'b7f9b458dde3c52760c5f6451f039eb7'),
(115, 'Chaitra D', 'drdchaitra@gmail.com', 'Mysore', 'Sri jayadeva institute of cardiovascular science and research center', NULL, NULL, '2021-05-29 19:23:18', '2021-05-29 19:23:18', '2021-05-29 19:49:31', 0, 'Abbott', 'b07787a9294b039551c98d44155525ce'),
(116, 'Niranjan reddy', 'drniranjan11@gmail.com', 'Kolar', 'RLJNH', NULL, NULL, '2021-05-29 19:23:24', '2021-05-29 19:23:24', '2021-05-29 20:53:24', 0, 'Abbott', 'cbb513ab443a18f4501c90c6c5217a60'),
(117, 'G J Sachin', 'sachingj80@gmail.com', 'Gulbarga', 'Jayadeva hospital', NULL, NULL, '2021-05-29 19:23:35', '2021-05-29 19:23:35', '2021-05-29 20:53:35', 0, 'Abbott', '6a7b6e6ae7a4f04011d59c3eec3c5595'),
(118, 'KRISHNA SARIN MS ', 'krishna588@gmail.com', 'Bengaluru  ', 'Specialist Hospital, Kalyan Nagar ', NULL, NULL, '2021-05-29 19:23:49', '2021-05-29 19:23:49', '2021-05-29 20:53:49', 0, 'Abbott', 'e5f950f3c3237c112ebdfae975e2a828'),
(119, 'Suganth', 'drngsuganth@gmail.com', 'Bengaluru', 'Jayadeva', NULL, NULL, '2021-05-29 19:23:50', '2021-05-29 19:23:50', '2021-05-29 20:53:50', 0, 'Abbott', '2da18ff6ffa05f517e93774673854b47'),
(120, 'Darshan Thakkar ', 'darshan_8277@yahoo.co.in', 'Bengaluru ', 'SJICR', NULL, NULL, '2021-05-29 19:23:50', '2021-05-29 19:23:50', '2021-05-29 20:53:50', 0, 'Abbott', 'f232b2198780ba30bdf343a0b480b47f'),
(121, 'phani teja', 'mundruphaniteja@gmail.com', 'Bengaluru ', 'Jayadeva ', NULL, NULL, '2021-05-29 19:23:54', '2021-05-29 19:23:54', '2021-05-29 20:53:54', 0, 'Abbott', '809ac389adc8a0e0ade73e7f666fa019'),
(122, 'Nagaraja Moorthy ', 'drnagaraj_moorthy@yahoo.com', 'Bangalore ', 'Sjicr ', NULL, NULL, '2021-05-29 19:24:29', '2021-05-29 19:24:29', '2021-05-29 20:54:29', 0, 'Abbott', 'be49f6e5fa20709167093e3ee84e3160'),
(123, 'Navendu Shah', 'navendushah@gmail.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-05-29 19:25:10', '2021-05-29 19:25:10', '2021-05-29 20:55:10', 0, 'Abbott', 'f1aa06f699c6bc8370c0d4d574f74a9f'),
(124, 'Dr. Satish K', 'drsatishkdm@yahoo.com', 'Bangalore', 'Jayadeva Hospital', NULL, NULL, '2021-05-29 19:25:12', '2021-05-29 19:25:12', '2021-05-29 19:25:52', 0, 'Abbott', '149c8dd920e1878e8163718d53e5faac'),
(125, 'Jagadesh Kalathil', 'jagadeesh.kalathil@abbott.com', 'Bangalore East', 'AV', NULL, NULL, '2021-05-29 19:25:42', '2021-05-29 19:25:42', '2021-05-29 20:55:42', 0, 'Abbott', '56c154ed1b4303d4ad2c6840b5337944'),
(126, 'Haritha', 'haritha1468@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-05-29 19:26:01', '2021-05-29 19:26:01', '2021-05-29 19:39:08', 0, 'Abbott', '33f505b1520bb9367c657619a7b2ca0c'),
(127, 'Dr. Satish K', 'drsatishkdm@yahoo.com', 'Bangalore', 'Jayadeva Hospital', NULL, NULL, '2021-05-29 19:26:26', '2021-05-29 19:26:26', '2021-05-29 19:27:52', 0, 'Abbott', 'e9a3faa3492d81c5d38990d94eb8ebf3'),
(128, 'Govind ', 'govind.auradkar@gmail.com', 'Bengaluru ', 'Sjicsr', NULL, NULL, '2021-05-29 19:26:27', '2021-05-29 19:26:27', '2021-05-29 20:56:27', 0, 'Abbott', '3dab52b534c4b505ecf5059abffb089b'),
(129, 'Raghu Ramegowda', 'drtrraghu@gmail.com', 'Bangalore', 'Rrmch', NULL, NULL, '2021-05-29 19:26:40', '2021-05-29 19:26:40', '2021-05-29 20:56:40', 0, 'Abbott', '2eba7dd8e539a30d0d99a89ffddf1fce'),
(130, 'Nishanth', 'kkk@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-05-29 19:27:17', '2021-05-29 19:27:17', '2021-05-29 20:57:17', 0, 'Abbott', 'b833288924458f62b7dbd66333990b68'),
(131, 'Yogesh Musale', 'yogesh.sitarammusale@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-29 19:27:26', '2021-05-29 19:27:26', '2021-05-29 20:57:26', 0, 'Abbott', '44ed7754ed266d17bbad3009decc8aff'),
(132, 'Dileep', 'harathiyenumala@gmail.com', 'bengaluru', 'sjic', NULL, NULL, '2021-05-29 19:28:10', '2021-05-29 19:28:10', '2021-05-29 20:58:10', 0, 'Abbott', '64b16212a46dffe71652134bf8382d82'),
(133, 'sreedhara ', 'kummas82@gmail.com', 'mysore ', 'sjicr ', NULL, NULL, '2021-05-29 19:28:12', '2021-05-29 19:28:12', '2021-05-29 20:58:12', 0, 'Abbott', '50bd32d3886d505f5fa71f754e0dad7c'),
(134, 'Dr Arun u', 'arun_i_u@yahoo.co.in', 'Bangalore ', 'SJICR ', NULL, NULL, '2021-05-29 19:28:13', '2021-05-29 19:28:13', '2021-05-29 20:58:13', 0, 'Abbott', '8de75ca916dea6f4356a1bb46dfcbc98'),
(135, 'Satish K', 'drsXXXXXXXX@yahoo.com', 'BENGALURU', 'Jayadeva hospital ', NULL, NULL, '2021-05-29 19:28:45', '2021-05-29 19:28:45', '2021-05-29 19:30:07', 0, 'Abbott', '57f632c28ba276e2672beb955ee0f18e'),
(136, 'Ajay', 'ajaypandita008@gmail.com', 'Bengaluru', 'SJICR', NULL, NULL, '2021-05-29 19:28:55', '2021-05-29 19:28:55', '2021-05-29 20:58:55', 0, 'Abbott', 'fb5296bcd363a22245c47e7eae304fff'),
(137, 'Dr. Satish K', 'drsatishkdm@yahoo.com', 'Bangalore', 'Jayadeva Hospital', NULL, NULL, '2021-05-29 19:29:55', '2021-05-29 19:29:55', '2021-05-29 20:59:55', 0, 'Abbott', 'b5f285ad30c8f021c187dbb76b9d355a'),
(138, 'Varun Marimuthu', 'varun.marimuthu@gmail.com', 'Coimbatore', 'Sjicsr', NULL, NULL, '2021-05-29 19:30:02', '2021-05-29 19:30:02', '2021-05-29 21:00:02', 0, 'Abbott', 'ac788cf4a28cd3ac7540b3b7f284eb92'),
(139, 'RAVI KISHORE', 'ravi_crazy7@yahoo.in', 'Bengaluru', 'SJICR', NULL, NULL, '2021-05-29 19:31:42', '2021-05-29 19:31:42', '2021-05-29 21:01:42', 0, 'Abbott', 'd4f3e428ac19aca714d8bd180eef66ae'),
(140, 'Hema Raveesh', 'hemaraveesh@yahoo.com', 'Mysuru ', 'Sjic', NULL, NULL, '2021-05-29 19:31:43', '2021-05-29 19:31:43', '2021-05-29 21:01:43', 0, 'Abbott', 'e39d8b08eb10608a926e7ee710d7a169'),
(141, 'Prajwal', 'prajwalbalakrishna@gmail.com', 'Bengaluru', 'SJIC&R', NULL, NULL, '2021-05-29 19:32:02', '2021-05-29 19:32:02', '2021-05-29 21:02:02', 0, 'Abbott', '65a941cfcd7628bf41a829267c8e315b'),
(142, 'Purushothama ', 'purushts@gmail.com', 'Mysore ', 'Sjicr ', NULL, NULL, '2021-05-29 19:32:27', '2021-05-29 19:32:27', '2021-05-29 21:02:27', 0, 'Abbott', 'bf277249e74f5eed71199351f7a76a1b'),
(143, 'Amith R', 'amith1ind@gmail.com', 'Bangalore', 'SJICR', NULL, NULL, '2021-05-29 19:33:37', '2021-05-29 19:33:37', '2021-05-29 21:03:37', 0, 'Abbott', 'a55109ea7b5b0ad7df712009c08cf6bf'),
(144, 'Dr. Nikhil B', 'nikhilbasavanagowda@gmail.com', 'Mysore', 'Sjicr mysore', NULL, NULL, '2021-05-29 19:33:50', '2021-05-29 19:33:50', '2021-05-29 21:03:50', 0, 'Abbott', 'f12a274711b5dede940d9948a233d153'),
(145, 'shashikant', 'nilangeshashi@gmail.com', 'Bangalore', 'Sjicr', NULL, NULL, '2021-05-29 19:34:56', '2021-05-29 19:34:56', '2021-05-29 21:04:56', 0, 'Abbott', '8377a74d9a42a2e23a835eec1b62cce4'),
(146, 'Purushothama ', 'purushts@gmail.com', 'Mysore ', 'Sjicr ', NULL, NULL, '2021-05-29 19:34:58', '2021-05-29 19:34:58', '2021-05-29 21:04:58', 0, 'Abbott', '54860e1c5a9c374da7ace1a7ae2ea5bb'),
(147, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 19:35:18', '2021-05-29 19:35:18', '2021-05-29 21:05:18', 0, 'Abbott', '9cbaea192bd20c0874db9af6adadaad3'),
(148, 'Dr Vivek gc ', 'gc.vivek@gmail.com', 'Bengaluru', 'Sjicsr', NULL, NULL, '2021-05-29 19:35:34', '2021-05-29 19:35:34', '2021-05-29 21:05:34', 0, 'Abbott', '7ef9fdba297414a081c67de9982c32fc'),
(149, 'Manohar Suranagi', 'manoharjs71@gmail.com', 'Bangalore ', 'Sri Jayadeva institute of cardiovascular sciences and research, Bangalore ', NULL, NULL, '2021-05-29 19:35:56', '2021-05-29 19:35:56', '2021-05-29 21:05:56', 0, 'Abbott', '83309caa5139b23cb9c5a42b6a3c74dc'),
(150, 'Girish', 'girishkol4262@gmail.com', 'Banglore', 'Jayadeva', NULL, NULL, '2021-05-29 19:36:46', '2021-05-29 19:36:46', '2021-05-29 21:06:46', 0, 'Abbott', '9b7b98f1b68f6d960f7dd2409fbc2a24'),
(151, 'Dr.surendhar', 'surenspelbound@gmail.com', 'Mysore', 'Sjicr', NULL, NULL, '2021-05-29 19:37:34', '2021-05-29 19:37:34', '2021-05-29 21:07:34', 0, 'Abbott', '722c1f07b92fc99129a54de280cc8261'),
(152, 'Subramanyam K', 'subramanyam70@yahoo.com', 'Bengaluru ', 'SJIC&R', NULL, NULL, '2021-05-29 19:37:44', '2021-05-29 19:37:44', '2021-05-29 21:07:44', 0, 'Abbott', '9de7df3f20b1157baf8f8ee4c6e948e4'),
(153, 'Dr Vivek gc ', 'gc.vivek@gmail.com', 'Bengaluru', 'Sjicsr', NULL, NULL, '2021-05-29 19:37:50', '2021-05-29 19:37:50', '2021-05-29 21:07:50', 0, 'Abbott', 'ee36213c6a0a8d33fdfb92efbd5353d9'),
(154, 'Manohar Suranagi', 'manoharjs71@gmail.com', 'Bangalore ', 'Sri Jayadeva institute of cardiovascular sciences and research, Bangalore.', NULL, NULL, '2021-05-29 19:38:26', '2021-05-29 19:38:26', '2021-05-29 21:08:26', 0, 'Abbott', '64855934b50f040853260d583ab7815d'),
(155, 'Vivek g c', 'gc.vivek@gmail.com', ' Bengaluru', 'Sjicsr', NULL, NULL, '2021-05-29 19:38:39', '2021-05-29 19:38:39', '2021-05-29 21:08:39', 0, 'Abbott', 'fdababd516561614f9b74deb43a80f5f'),
(156, 'Satvic CM', 'drsatvic@gmail.com', 'Bangalore ', 'SJICR', NULL, NULL, '2021-05-29 19:39:00', '2021-05-29 19:39:00', '2021-05-29 21:09:00', 0, 'Abbott', '03334c5f3aa887fb0e30bfb30913ac4e'),
(157, 'Abraham Paul', 'abrahampluspaul@gmail.com', 'Ernakulam', 'Sjicr', NULL, NULL, '2021-05-29 19:39:09', '2021-05-29 19:39:09', '2021-05-29 21:09:09', 0, 'Abbott', 'fe4537d0cf11ecf31099495f12da6955'),
(158, 'Haritha', 'haritha1468@gmail.com', 'Bangalore', 'Sjicr', NULL, NULL, '2021-05-29 19:39:37', '2021-05-29 19:39:37', '2021-05-29 21:09:37', 0, 'Abbott', 'fd8d6f2759308524b44e66700ce4f0e6'),
(159, 'Chamarajnagar M Nagesh', 'drnageshcm@yahoo.com', 'Bangalore ', 'Sri Jayadeva Hospital ', NULL, NULL, '2021-05-29 19:39:43', '2021-05-29 19:39:43', '2021-05-29 21:09:43', 0, 'Abbott', 'e1434a9787cc477132c96d8a56b27e99'),
(160, 'Hariprasad Rao', 'prasadhari88@gmail.com', 'Banglore', 'Sjicr', NULL, NULL, '2021-05-29 19:39:43', '2021-05-29 19:39:43', '2021-05-29 21:09:43', 0, 'Abbott', '424c58b8361f2286eb5a7d5451ec05bc'),
(161, 'RAVI KISHORE', 'ravi_crazy7@yahoo.in', 'Bengaluru', 'SJICR', NULL, NULL, '2021-05-29 19:40:14', '2021-05-29 19:40:14', '2021-05-29 21:10:14', 0, 'Abbott', '94939d98145c4a39a53a5dac45274ca8'),
(162, 'Gaurav Agarwal', 'gaurav9087@gmail.com', 'Bengaluru', 'Sri Jayadeva Institute of Cardiovascular Sciences & Research', NULL, NULL, '2021-05-29 19:40:28', '2021-05-29 19:40:28', '2021-05-29 21:10:28', 0, 'Abbott', 'e27b342f68071ae842a254dde157edf9'),
(163, 'CN MANJUNATH ', 'drcnmanjunayth@gmail.com', 'Bangalore ', 'SJICR ', NULL, NULL, '2021-05-29 19:41:02', '2021-05-29 19:41:02', '2021-05-29 21:11:02', 0, 'Abbott', '5b0cc907a7b517160e0b39bb8cad0573'),
(164, 'Shadman Ali', 'drshadmanali@gmail.com', 'Bengaluru', 'SJICR', NULL, NULL, '2021-05-29 19:41:24', '2021-05-29 19:41:24', '2021-05-29 21:11:24', 0, 'Abbott', '4c7ec84570a228c012e202c49bb4fecf'),
(165, 'DR VENKATESH TK ', 'venkateshtekur@yahoo.com', 'Bangalore ', 'Apollo Hospital ', NULL, NULL, '2021-05-29 19:42:19', '2021-05-29 19:42:19', '2021-05-29 21:12:19', 0, 'Abbott', '57866164d5ea024d8d22e71ce2045762'),
(166, 'Dr Shankar sp Shankar sira', 'spshankarsira@gmail.com', 'Bangalore', 'JAYADEVA HOSPITAL', NULL, NULL, '2021-05-29 19:42:45', '2021-05-29 19:42:45', '2021-05-29 21:12:45', 0, 'Abbott', 'ee58b0b59ffaa218f05327dc62d6234d'),
(167, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 19:45:11', '2021-05-29 19:45:11', '2021-05-29 21:15:11', 0, 'Abbott', '3f271ba86a5c7788056ed7fa20e2230c'),
(168, 'NARAYANEE RAJASEKARAN', 'narayaneerajasekaran@yahoo.com', 'Bangalore', 'Sri Jayadeva Institute ', NULL, NULL, '2021-05-29 19:46:16', '2021-05-29 19:46:16', '2021-05-29 21:16:16', 0, 'Abbott', '4e4f27e4a69cd9ac1710987fc1c06441'),
(169, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 19:46:46', '2021-05-29 19:46:46', '2021-05-29 21:16:46', 0, 'Abbott', 'c93b2954bc3c47a30ee8eaf7ea0ce09c'),
(170, 'LOKESH PRAMOD CHAUDHARI', 'lokeshataipg@rediffmail.com', 'banglore', 'Sjic', NULL, NULL, '2021-05-29 19:46:54', '2021-05-29 19:46:54', '2021-05-29 21:16:54', 0, 'Abbott', '14a3716593d00c23e26d4776fe331cf3'),
(171, 'Vikram B Kolhari', 'vkolhari@yahoo.com', 'Bangalore ', 'Apollo hospital ', NULL, NULL, '2021-05-29 19:47:48', '2021-05-29 19:47:48', '2021-05-29 21:17:48', 0, 'Abbott', 'df88ee9fc874226c82190d265a184236'),
(172, 'Puneet Shah', 'puneetishere2050@gmail.com', 'Bengaluru (Bangalore) Urban', 'Sjicr', NULL, NULL, '2021-05-29 19:48:17', '2021-05-29 19:48:17', '2021-05-29 21:18:17', 0, 'Abbott', '62f2f31dd8e089082a0475fec9d2c277'),
(173, 'RONAK BHANDARI ', 'dr.ronakbhandari@gmail.com', 'BANGALORE ', 'SJICR ', NULL, NULL, '2021-05-29 19:48:41', '2021-05-29 19:48:41', '2021-05-29 21:18:41', 0, 'Abbott', 'b61a73745cf6de1040dfd2be936a27d3'),
(174, 'Chethan A', 'chethananjan5@gmail.com', 'BANGALORE', 'SRI Jayadeva institute of cardiovascular science and research', NULL, NULL, '2021-05-29 19:50:01', '2021-05-29 19:50:01', '2021-05-29 21:20:01', 0, 'Abbott', 'a18bb0197f586beb974dbd4d4fc27d75'),
(175, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 19:50:40', '2021-05-29 19:50:40', '2021-05-29 21:20:40', 0, 'Abbott', '2cdb38669e53e0f76d4f07ba0de16715'),
(176, 'Sharad Masudi', 'sharad.masudi@gmail.com', 'Bengaluru', 'Sri Jayadeva institute of Cardiology', NULL, NULL, '2021-05-29 19:50:41', '2021-05-29 19:50:41', '2021-05-29 21:20:41', 0, 'Abbott', '52974b199cd0042cc9595b8376b1041a'),
(177, 'Sharathbabu N M', 'nmsbabu18@yahoo.com', 'Mysuru', 'SJICSR', NULL, NULL, '2021-05-29 19:52:31', '2021-05-29 19:52:31', '2021-05-29 21:22:31', 0, 'Abbott', '19074258f851fd3e2bf749efc2e3858e'),
(178, 'Anand', 'anandmmc030485@yahoo.co.in', 'Bangalore', 'Jaydeva', NULL, NULL, '2021-05-29 19:53:01', '2021-05-29 19:53:01', '2021-05-29 21:23:01', 0, 'Abbott', '032b6acd362f07edfdce2cb6f5ebd7e6'),
(179, 'Akash jain', 'drakashjain92@gmail.com', 'BangalOre', 'Sjicsr', NULL, NULL, '2021-05-29 19:53:27', '2021-05-29 19:53:27', '2021-05-29 19:59:19', 0, 'Abbott', '4a213ce94bfae0b03feca54f01cd2dd2'),
(180, 'Surendhar', 'surenspelbound@gmail.com', 'Mysore', 'Sjicr', NULL, NULL, '2021-05-29 19:53:38', '2021-05-29 19:53:38', '2021-05-29 21:23:38', 0, 'Abbott', 'dd33a1e374b4a3fb8b88b3893c312187'),
(181, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 19:53:45', '2021-05-29 19:53:45', '2021-05-29 21:23:45', 0, 'Abbott', 'dccebad23ad3562cf51d1f4a64ec1ebd'),
(182, 'Srinivas', 'rede237@gmail.com', 'Bellary', 'Jayadeva hospital', NULL, NULL, '2021-05-29 19:54:15', '2021-05-29 19:54:15', '2021-05-29 20:18:59', 0, 'Abbott', '35ae5b38da3da9d1d8d97751ed2f04e5'),
(183, 'Surendhar', 'surenspelbound@gmail.com', 'Bangalore ', 'Sri jayadeva cardiovascular institute ', NULL, NULL, '2021-05-29 19:55:08', '2021-05-29 19:55:08', '2021-05-29 21:25:08', 0, 'Abbott', '28954e709be286892bedadb113ab2e82'),
(184, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 19:58:14', '2021-05-29 19:58:14', '2021-05-29 21:28:14', 0, 'Abbott', '8656b0b3a5684703e3f073e79810bf03'),
(185, 'RONAK ', 'dr.ronakbhandari@gmail.com', 'Indore', 'SJICR ', NULL, NULL, '2021-05-29 19:58:54', '2021-05-29 19:58:54', '2021-05-29 21:28:54', 0, 'Abbott', '6a4d5426beb53b4e1d5c8ef925876b91'),
(186, 'Chaitra D', 'drdchaitra@gmail.com', 'Mysore', 'Sri jayadeva institute of cardiovascular science and research center', NULL, NULL, '2021-05-29 19:58:55', '2021-05-29 19:58:55', '2021-05-29 20:11:09', 0, 'Abbott', '8aca2f1caebe5d2e4771b7db66df7345'),
(187, 'Swathi', 'pinky.preethi24@gmail.com', 'Bangalore', 'Apollo', NULL, NULL, '2021-05-29 19:59:46', '2021-05-29 19:59:46', '2021-05-29 21:29:46', 0, 'Abbott', 'c7150fcc0ec257499644adffbf5d8be2'),
(188, 'Sharathbabu N M', 'nmsbabu18@yahoo.com', 'Mysuru', 'SJICSR', NULL, NULL, '2021-05-29 19:59:55', '2021-05-29 19:59:55', '2021-05-29 21:29:55', 0, 'Abbott', 'd553a474bf2422ff8633deef4b040a3c'),
(189, 'Hema Raveesh', 'hemaraveesh@yahoo.com', 'Mysuru ', 'Sjic', NULL, NULL, '2021-05-29 20:00:04', '2021-05-29 20:00:04', '2021-05-29 21:30:04', 0, 'Abbott', '1efc60f559fb6e186953d706537cc111'),
(190, 'Purushothama ', 'purushts@gmail.com', 'Mysore ', 'Sjicr ', NULL, NULL, '2021-05-29 20:02:33', '2021-05-29 20:02:33', '2021-05-29 21:32:33', 0, 'Abbott', '797453c4bdcb98cde1ee4e7fbd91200f'),
(191, 'Mohammed saif khan', 'mohammad.s.khan@abbott.com', 'BangLore ', 'Abbott', NULL, NULL, '2021-05-29 20:02:55', '2021-05-29 20:02:55', '2021-05-29 21:32:55', 0, 'Abbott', '43319083e52d260071ab34ca92c18383'),
(192, 'abraham', 'abrahampluspaul@gmail.com', 'bangalore', 'sjicr', NULL, NULL, '2021-05-29 20:03:31', '2021-05-29 20:03:31', '2021-05-29 21:33:31', 0, 'Abbott', 'c33e8c3b43a4acd6c6dd00ec17f1e28c'),
(193, 'Chethan A', 'chethananjan5@gmail.com', 'BANGALORE ', 'SRI Jayadeva institute of cardiovascular science and research', NULL, NULL, '2021-05-29 20:03:36', '2021-05-29 20:03:36', '2021-05-29 21:33:36', 0, 'Abbott', '750f30284eedf106e83991a33fc8c303'),
(194, 'PADMAJA', 'padmar091@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-05-29 20:03:45', '2021-05-29 20:03:45', '2021-05-29 21:33:45', 0, 'Abbott', '34499fe40e0cfd07eac9fd0c0dfb4ece'),
(195, 'Nagaraja Moorthy ', 'drnagaraj_moorthy@yahoo.com', 'Bangalore ', 'SJIC ', NULL, NULL, '2021-05-29 20:03:54', '2021-05-29 20:03:54', '2021-05-29 21:33:54', 0, 'Abbott', '4da9b09ca1dea9140727d4e43e87dd9a'),
(196, 'Naga Srinivaas', 'drakondi.ns@gmail.gom', 'Bengaluru ', 'Manipal ', NULL, NULL, '2021-05-29 20:04:04', '2021-05-29 20:04:04', '2021-05-29 21:34:04', 0, 'Abbott', 'c5b0c0bff17189695f931b9c7e87ff8f'),
(197, 'Yeriswamy Mogalahally channabasappa', 'drswim2003@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-05-29 20:04:19', '2021-05-29 20:04:19', '2021-05-29 21:34:19', 0, 'Abbott', '445510f983d6fd24ec22fe3ca98b636b'),
(198, 'Dr. Vikram B K', 'vkolhari@yahoo.com', 'Bangalore ', 'Apollo hospital ', NULL, NULL, '2021-05-29 20:06:09', '2021-05-29 20:06:09', '2021-05-29 20:11:15', 0, 'Abbott', 'b7bddca32b7547a932206de94b196f1f'),
(199, 'Yogesh  Musale', 'yogesh.sitarammusale@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-29 20:06:39', '2021-05-29 20:06:39', '2021-05-29 21:36:39', 0, 'Abbott', 'd141974cf4a9429c6b711136625041c3'),
(200, 'Lavanya', 'lavanyalavu1018@gmail.com', 'Bengaluru Urban', 'Jayadeva', NULL, NULL, '2021-05-29 20:06:46', '2021-05-29 20:06:46', '2021-05-29 21:36:46', 0, 'Abbott', '6fe90e58cfd2d01e73ca33c1d15aedb5'),
(201, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 20:07:50', '2021-05-29 20:07:50', '2021-05-29 21:37:50', 0, 'Abbott', '3ebca48b91c0b949d0f4421eb9ea530b'),
(202, 'Shilpa Jayaprakash', 'shilpajv@yahoo.co.in', 'shilpajv@yahoo.co.in', 'Jayadeva', NULL, NULL, '2021-05-29 20:07:59', '2021-05-29 20:07:59', '2021-05-29 21:37:59', 0, 'Abbott', '85e0a0a62012550f50f84604dd9684f3'),
(203, 'Yogesh Musale', 'yogesh.sitarammusale@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-29 20:08:01', '2021-05-29 20:08:01', '2021-05-29 21:38:01', 0, 'Abbott', '0dd6497aa924de0f871538a4896596e7'),
(204, 'RAVI KISHORE', 'ravi_crazy7@yahoo.in', 'Bengaluru', 'SJICR', NULL, NULL, '2021-05-29 20:08:41', '2021-05-29 20:08:41', '2021-05-29 21:38:41', 0, 'Abbott', 'e0035ad32962103bf69a2c2890a943dd'),
(205, 'Yogesh Musale', 'yogesh.sitarammusale@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-29 20:08:54', '2021-05-29 20:08:54', '2021-05-29 20:09:14', 0, 'Abbott', '09630786b3c8f1d2bb7b601c165aad43'),
(206, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-29 20:09:53', '2021-05-29 20:09:53', '2021-05-29 21:39:53', 0, 'Abbott', 'f406393f75f82f5bda89570c3b9cbf5c'),
(207, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 20:13:40', '2021-05-29 20:13:40', '2021-05-29 21:43:40', 0, 'Abbott', 'f552c936512d7dc7f722bb35ce2864c6'),
(208, 'Prajwal', 'prajwalbalakrishna@gmail.com', 'Bengaluru', 'SJIC&R', NULL, NULL, '2021-05-29 20:13:55', '2021-05-29 20:13:55', '2021-05-29 21:43:55', 0, 'Abbott', 'c164707d495dd48aec4354555086e38e'),
(209, 'Ck', 'chetanhb09@gmail.com', 'Bangalore', 'Sjicrs', NULL, NULL, '2021-05-29 20:14:14', '2021-05-29 20:14:14', '2021-05-29 21:44:14', 0, 'Abbott', '65fdf113272648d0df08934d33f1c0ff'),
(210, 'dr.mahesh', 'maheshvaidya26@gmail.com', 'SHIMOGA', 'HRUDAY SPECIALITY CLINIC', NULL, NULL, '2021-05-29 20:14:22', '2021-05-29 20:14:22', '2021-05-29 21:44:22', 0, 'Abbott', '2b28e5f8a8d3afaaed17fbbb90ec6679'),
(211, 'Pooja', 'pooja@coact.co.in', 'Mumbai', 'Test', NULL, NULL, '2021-05-29 20:14:55', '2021-05-29 20:14:55', '2021-05-29 21:44:55', 0, 'Abbott', 'cf2b75f70b51e06b6047e35c2e680006'),
(212, 'Chaitra D', 'drdchaitra@gmail.com', 'Mysore', 'Sri jayadeva institute of cardiovascular science and research center', NULL, NULL, '2021-05-29 20:14:57', '2021-05-29 20:14:57', '2021-05-29 21:44:57', 0, 'Abbott', '490d659476d9d891192b44934933f182'),
(213, 'Anindya', 'anindyaagmc@gmail.com', 'banglore', 'SJICR', NULL, NULL, '2021-05-29 20:15:06', '2021-05-29 20:15:06', '2021-05-29 21:45:06', 0, 'Abbott', '405e8ae26823bd115766c8751834c8af'),
(214, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 20:16:26', '2021-05-29 20:16:26', '2021-05-29 21:46:26', 0, 'Abbott', 'b015427a0dede1519d0efb0b55e9b88c'),
(215, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 20:16:37', '2021-05-29 20:16:37', '2021-05-29 21:46:37', 0, 'Abbott', '3439d1aa15e0016371e8c09c5b1b1077'),
(216, 'Hema Raveesh', 'hemaraveesh@yahoo.com', 'Mysuru ', 'Sjic', NULL, NULL, '2021-05-29 20:18:10', '2021-05-29 20:18:10', '2021-05-29 21:48:10', 0, 'Abbott', '391bae70fca3f1c65e269c1011247bd9'),
(217, 'Dr. B. K. Geetha', 'drbkgeetha@gmail.com', 'Bangalore', 'Sri Jayadeva institute of cardiology and Research ', NULL, NULL, '2021-05-29 20:19:13', '2021-05-29 20:19:13', '2021-05-29 20:25:29', 0, 'Abbott', 'a083093f481300bf24587640d5f60ef1'),
(218, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 20:21:56', '2021-05-29 20:21:56', '2021-05-29 21:51:56', 0, 'Abbott', '0808a4d0a241805b009dc9794d5eaf0c'),
(219, 'Dr CNM ', 'drcnmanjunath@gmail.com', 'Bangalore ', 'SJICR', NULL, NULL, '2021-05-29 20:24:27', '2021-05-29 20:24:27', '2021-05-29 21:54:27', 0, 'Abbott', '5089b24d03f4d4311b1bad2b452a0a21'),
(220, 'CNM ', 'drcnmanjunath@gmail.com', 'Bangalore ', 'SJICR', NULL, NULL, '2021-05-29 20:25:43', '2021-05-29 20:25:43', '2021-05-29 21:55:43', 0, 'Abbott', '8da7a3bf8fe7389fd427c2f4b30973e0'),
(221, 'Beeresha ', 'beereshp@yahoo.co.in', 'Bengaluru ', 'SJICR ', NULL, NULL, '2021-05-29 20:26:31', '2021-05-29 20:26:31', '2021-05-29 21:56:31', 0, 'Abbott', '0d397e1c6b6f53b60f6f484f3c6550ab'),
(222, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 20:27:38', '2021-05-29 20:27:38', '2021-05-29 21:57:38', 0, 'Abbott', '827f76046c33427e59b3f333554f0921'),
(223, 'Nagamani A C', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva hospital ', NULL, NULL, '2021-05-29 20:28:43', '2021-05-29 20:28:43', '2021-05-29 21:58:43', 0, 'Abbott', 'd45f153966c28d2bc3c76ee77c4bf382'),
(224, 'RONAK ', 'dr.ronakbhandari@gmail.com', 'Bangalore ', 'SJICR ', NULL, NULL, '2021-05-29 20:29:28', '2021-05-29 20:29:28', '2021-05-29 21:59:28', 0, 'Abbott', 'a0e7bd109f1fdc722aefd65e200d11a7'),
(225, 'Yogesh  Musale', 'yogesh.sitarammusale@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-29 20:30:10', '2021-05-29 20:30:10', '2021-05-29 20:59:32', 0, 'Abbott', '7ccf4f38f6d5c287ded85f07f6281585'),
(226, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 20:30:19', '2021-05-29 20:30:19', '2021-05-29 20:32:32', 0, 'Abbott', 'e07d687a09b76ae2f2e9b10991297553'),
(227, 'Praveen kumar', 'jipmer.praveen@gmail.com', 'kolar', 'T', NULL, NULL, '2021-05-29 20:31:16', '2021-05-29 20:31:16', '2021-05-29 22:01:16', 0, 'Abbott', '3fe37a1a289c99b00be401e2fcfd108b'),
(228, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-05-29 20:32:28', '2021-05-29 20:32:28', '2021-05-29 20:58:48', 0, 'Abbott', 'f34e6c0f1a65861d7b6ea6366ce50834'),
(229, 'KRISHNA SARIN MS ', 'krishna588@gmail.com', 'Bengaluru  ', 'Specialist Hospital, Kalyan Nagar ', NULL, NULL, '2021-05-29 20:33:18', '2021-05-29 20:33:18', '2021-05-29 22:03:18', 0, 'Abbott', '8d5d02c10b0138d8352de1761aef1db3'),
(230, 'Navendu Shah', 'navendushah@gmail.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-05-29 20:33:51', '2021-05-29 20:33:51', '2021-05-29 22:03:51', 0, 'Abbott', '132bd38e59c8d159cd8d42f033807e45'),
(231, 'Lingaiah', 'lingaiah.gaddi@abbott.com', 'Hyderabad', 'Abbott', NULL, NULL, '2021-05-29 20:34:06', '2021-05-29 20:34:06', '2021-05-29 22:04:06', 0, 'Abbott', 'a4675f16498a8f747421c3f13782b4cb'),
(232, 'Yogesh Musale', 'yogesh.sitarammusale@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-29 20:34:15', '2021-05-29 20:34:15', '2021-05-29 22:04:15', 0, 'Abbott', 'f77ab3120864a401b5efcaf3df599aeb'),
(233, 'Varun Marimuthu', 'varun.marimuthu@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-05-29 20:35:40', '2021-05-29 20:35:40', '2021-05-29 22:05:40', 0, 'Abbott', 'a98b8aa8500943b8670713afe5bcec84'),
(234, 'Nagaraja Moorthy', 'drnagaraj_moorthy@yahoo.com', 'Bangalore ', 'Sjicr ', NULL, NULL, '2021-05-29 20:37:02', '2021-05-29 20:37:02', '2021-05-29 22:07:02', 0, 'Abbott', '4c65af5f70eab6ae70044de2790f0313'),
(235, 'PADMAJA', 'padmar091@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-05-29 20:38:09', '2021-05-29 20:38:09', '2021-05-29 20:59:23', 0, 'Abbott', '9a90d54374fa568a929b1d54ed9d1d86'),
(236, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 20:38:37', '2021-05-29 20:38:37', '2021-05-29 22:08:37', 0, 'Abbott', '2ea7634388f019499426d4e869010904'),
(237, 'Phani ', 'mundruphaniteja@gmail.com', 'Bengaluru ', 'Jayadeva', NULL, NULL, '2021-05-29 20:40:11', '2021-05-29 20:40:11', '2021-05-29 22:10:11', 0, 'Abbott', '375cc8d528f2f7271b44d50d49656259'),
(238, 'Navendu Shah', 'navendushah@gmail.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-05-29 20:41:21', '2021-05-29 20:41:21', '2021-05-29 20:51:00', 0, 'Abbott', '530f7a1d22ea513edb3f53e3c4a2d600'),
(239, 'Yeriswamy Mogalahally channabasappa', 'drswim2003@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-05-29 20:42:51', '2021-05-29 20:42:51', '2021-05-29 22:12:51', 0, 'Abbott', '304ee9d03f4a0db932aacf655c0e60ba'),
(240, 'Prajwal', 'prajwalbalakrishna@gmail.com', 'Bengaluru', 'SJIC&R', NULL, NULL, '2021-05-29 20:48:08', '2021-05-29 20:48:08', '2021-05-29 22:18:08', 0, 'Abbott', '08338752742a19ca004dedb72c2ed9e7'),
(241, 'Purushothama ', 'purushts@gmail.com', 'Mysore ', 'Sjicr ', NULL, NULL, '2021-05-29 20:48:48', '2021-05-29 20:48:48', '2021-05-29 22:18:48', 0, 'Abbott', '840120069acb24eddd26b6a7b2348f8f'),
(242, 'Navendu Shah', 'navendu.shah@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-05-29 20:51:30', '2021-05-29 20:51:30', '2021-05-29 22:21:30', 0, 'Abbott', '6ab5a7695cb539a4e5e2c1bb050ef287');
INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(243, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 20:51:56', '2021-05-29 20:51:56', '2021-05-29 22:21:56', 0, 'Abbott', '0b2a4408f424b168e6d7479a2df1ea66'),
(244, 'Rajendra', 'drrajendraph@gmail.com', 'Bangalore', 'Jayadeva hospital ', NULL, NULL, '2021-05-29 20:55:03', '2021-05-29 20:55:03', '2021-05-29 20:58:54', 0, 'Abbott', 'b754776bb72a8ecf6b753d98504a9349'),
(245, 'Dr sunil Christopher ', 'drsunilchristophert@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-05-29 20:57:59', '2021-05-29 20:57:59', '2021-05-29 22:27:59', 0, 'Abbott', 'a773aac6467dd7967fba7e8df5ec9a4d'),
(246, 'Praveen kumar', 'jipmer.praveen@gmail.com', 'kolar', 'T', NULL, NULL, '2021-05-29 20:59:48', '2021-05-29 20:59:48', '2021-05-29 21:00:07', 0, 'Abbott', 'cbd1018d702d0ca1d70294b12c54f6a6'),
(247, 'Praveen kumar', 'jipmer.praveen@gmail.com', 'kolar', 'Y', NULL, NULL, '2021-05-29 21:00:22', '2021-05-29 21:00:22', '2021-05-29 22:30:22', 0, 'Abbott', '1664164426e1fec6ed6ba5bdd5ce93ca'),
(248, 'Namratha g', 'Namratha.sjic@gmail.com', 'Bangalore ', 'jayadeva', NULL, NULL, '2021-05-29 21:00:39', '2021-05-29 21:00:39', '2021-05-29 22:30:39', 0, 'Abbott', '1037fbd57955be00670a3c1130f27e99'),
(249, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-29 21:01:02', '2021-05-29 21:01:02', '2021-05-29 21:01:30', 0, 'Abbott', 'fb3ea15ab581ecadcf8b8dcdfbd78659'),
(250, 'Surendhar ', 'surenspelbound@gmail.com', 'Bangalore ', 'Sjicr', NULL, NULL, '2021-05-29 21:01:09', '2021-05-29 21:01:09', '2021-05-29 22:31:09', 0, 'Abbott', 'c1995ef8ff43de3edf876204b24ab0af'),
(251, 'Namratha g', 'Namratha.sjic@gmail.com', 'Bangalore ', 'jayadeva', NULL, NULL, '2021-05-29 21:02:28', '2021-05-29 21:02:28', '2021-05-29 22:32:28', 0, 'Abbott', '670a4d9bbd1c22314326d03185aa3afe'),
(252, 'Namratha g', 'Namratha.sjic@gmail.com', 'Bangalore ', 'jayadeva', NULL, NULL, '2021-05-29 21:02:28', '2021-05-29 21:02:28', '2021-05-29 22:32:28', 0, 'Abbott', 'a0349ceb38cd6548fca2f10d10205258'),
(253, 'Thirumaleshwara m', 'thiru.mmc05@gmail.com', 'Bangalore', 'SJCR', NULL, NULL, '2021-05-29 21:03:49', '2021-05-29 21:03:49', '2021-05-29 22:33:49', 0, 'Abbott', 'da04cba5bb58cc47aa9fea1dde2d568e'),
(254, 'Dr L Sridhar', 'srisrisastry@gmail.com', 'Bangalore', 'Sri Jayadeva Institute of Cardiovascular Sciences & Research', NULL, NULL, '2021-05-29 21:08:07', '2021-05-29 21:08:07', '2021-05-29 22:38:07', 0, 'Abbott', '47c67dd2bb9d2b2516e95fb389533b40'),
(255, 'Kuldeep', 'krtworld@gmail.com', 'Mumbai 400012', 'SJICR', NULL, NULL, '2021-05-29 21:14:19', '2021-05-29 21:14:19', '2021-05-29 21:14:29', 0, 'Abbott', '6882c7d57f8e4372054b49228c5fba73'),
(256, 'Disha', 'dishaganeshshetty@gmail.com', 'Bangalore ', 'Jayadeva ', NULL, NULL, '2021-05-29 21:24:20', '2021-05-29 21:24:20', '2021-05-29 22:54:20', 0, 'Abbott', '8d294bb13727af23f498161ea44cdb6c'),
(257, 'LOKESH PRAMOD CHAUDHARI', 'lokeshataipg@rediffmail.com', 'banglore', 'Sjic', NULL, NULL, '2021-05-29 21:24:41', '2021-05-29 21:24:41', '2021-05-29 22:54:41', 0, 'Abbott', '71bb0895728c16e020c75c7c11a05e6b'),
(258, 'V n', 'veenananjappa@yahoo.co.ih', 'Mysore', 'Sjicsr', NULL, NULL, '2021-05-30 07:29:51', '2021-05-30 07:29:51', '2021-05-30 08:59:51', 0, 'Abbott', '4ef1a831a88fa2193a38461855ee2039'),
(259, 'Arun James', 'arunjames6683@gmail.com', 'BANGALORE', 'AV', NULL, NULL, '2021-05-30 08:12:03', '2021-05-30 08:12:03', '2021-05-30 09:42:03', 0, 'Abbott', 'e24aef6cd1553602045de1b133f372c0'),
(260, 'Abdul Rahman', 'drabdulr1@gmail.com', 'Bangalore', 'Jayadeva hospital', NULL, NULL, '2021-05-30 19:23:48', '2021-05-30 19:23:48', '2021-05-30 20:53:48', 0, 'Abbott', 'ae33a95cf98a3cc399d333c7f9c8daa4'),
(261, 'PADMAJA', 'padmar091@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-05-30 22:50:33', '2021-05-30 22:50:33', '2021-05-31 00:20:33', 0, 'Abbott', '4b898c6409aabcf92104b66e3c670a79'),
(262, 'Veena Nanjappa', 'veenananjappa@yahoo.co.in', 'Mysore ', 'Sjicsr', NULL, NULL, '2021-06-02 05:52:28', '2021-06-02 05:52:28', '2021-06-02 07:22:28', 0, 'Abbott', '30920671eee182af4466f0fe47693da4'),
(263, 'Arun James', 'arunjames6683@gmail.com', 'BANGALORE', 'AV', NULL, NULL, '2021-06-02 16:32:42', '2021-06-02 16:32:42', '2021-06-02 18:02:42', 0, 'Abbott', '11544fd80eee4bfba22a9b1ae07e4377');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=264;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
